$ ->
  $(".logo").attr "src", $(".logo").attr("data-fallback")  unless Modernizr.inlinesvg
  $(".show-grid").on "click", (e) ->
    e.preventDefault()
    $(this).toggleClass "active"
    $(".container").toggleClass "grid-color"

  $("#tabs div").hide()
  $("#tabs div:first").show()
  $("#tab-links li:first").addClass "active-tab"
  $("#tab-links li a").click (e) ->
    e.preventDefault()
    $("#tab-links li").removeClass "active-tab"
    $(this).parent().addClass "active-tab"
    currentTab = $(this).attr("href")
    $("#tabs div").hide()
    $(currentTab).show()

  $(".get-title").each ->
    $(this).text $(this).closest("aside").find("h4").text()

  $("nav a").click ->
    elementId = $(this).attr("href")
    $("html, body").animate
      scrollTop: $(elementId).offset().top
    , "slow"
    false
  $("#scrollTop").click, (e) ->
    $("html, body").animate
      scrollTop: $("html, body").offset().top
    , 2000