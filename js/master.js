$(function(){
	if (!Modernizr.inlinesvg) {
        $('.logo').attr('src', $('.logo').attr('data-fallback'));
    }
    $(".show-grid").on("click", function(e){
		e.preventDefault();
		$(this).toggleClass("active");
		$(".container").toggleClass("grid-color");
    });
	$('#tabs div').hide();
	$('#tabs div:first').show();
	$('#tab-links li:first').addClass('active-tab');
	$('#tab-links li a').click(function(e){
		e.preventDefault();
		$("#tab-links li").removeClass('active-tab');
		$(this).parent().addClass("active-tab");
		var currentTab = $(this).attr('href');
		$('#tabs div').hide();
		$(currentTab).show();
	});
	$('.get-title').each(function () {
		$(this).text($(this).closest('aside').find('h4').text()); 
	});
	$("nav a").click(function(){
		var elementId = $(this).attr("href");
		$("html, body").animate({
			scrollTop: $(elementId).offset().top
		}, "slow");
		return false;
	});
	$("#scrollTop").click(function(e){
		e.preventDefault();
		$("html, body").animate({
			scrollTop: $("html, body").offset().top
		}, 2000);
	});
	$('.email').each(function(i){
		var protectedEmail = $(this).html();
		protectedEmail = protectedEmail.replace(" [snabela] ","@");
		protectedEmail = protectedEmail.replace(" [punkt] ",".");
		
		$(this)
		  .html(protectedEmail)
		  .replaceWith("<a href=\"mailto:"+$(this).text()+"\">"+$(this).text()+"</a>");
	});
});