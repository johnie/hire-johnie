<!DOCTYPE HTML>
<html>
<head>

	<meta name="author" content="Johnie Hjelm">
	<meta name="description" content="Johnie Hjelm - Passioned Front-end Developer">
	<meta name="keywords" content="Johnie, Hjelm, Front-end, PHP, jQuery, CSS, Sass, Coffeescript, WordPress, Developer, Designer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

	<script type="text/javascript" src="http://www.google.com/jsapi"></script><script type="text/javascript">google.load("jquery", "1");</script>
	<script type="text/javascript" src="js/modernizr.js"></script>

	<!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<title>Johnie Hjelm - Passioned Front-end Developer</title>

<meta charset="UTF-8" />
</head>

<body>

	<div class="container">
		<header>
			<img class="logo" src="images/logo.svg" width="45" height="45" alt="logo" data-fallback"images/logo.png">
			<h1>Johnie Hjelm</h1>
		</header>

		<nav>
			<ul>
				<li><a href="#projects">Projects</a></li>
				<li><a href="#about">About me</a></li>
				<li><a href="#cv">My CV</a></li>
				<li><a href="http://johniehjelm.me">Blog</a></li>
				<li><a href="mailto:johniehjelm@me.com">Hire me</a></li>
			</ul>
		</nav>

		<div class="clearfix"></div>
	
		<h2>I am Johnie. A designer and developer who loves when technology meets design.</h2>
	
		<div class="clearfix"></div>

		<div id="projects">
			<h3>Projects</h3>

			<hr />

			<ul id="works">
				<li>
					<figure>
						<img src="http://placehold.it/900x600&text=Project+image" width="450" height="300">
					</figure>

					<aside>
						<h4>Hello world</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nam fugit sapiente tempora incidunt nemo doloribus in vitae consectetur sequi quas officiis ut unde maiores amet laborum deleniti enim assumenda!
						</p>

						<a href="#" class="visit-work">Visit <span class="get-title">Projects title</span></a>
					</aside>
					<div class="clearfix"></div>
				</li>
				<li>
					<figure>
						<img src="http://placehold.it/900x600&text=Project+image" width="450" height="300">
					</figure>

					<aside>
						<h4>Projects title</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nam fugit sapiente tempora incidunt nemo doloribus in vitae consectetur sequi quas officiis ut unde maiores amet laborum deleniti enim assumenda!
						</p>

						<a href="#" class="visit-work">Visit <span class="get-title">Projects title</span></a>
					</aside>
					<div class="clearfix"></div>
				</li>
				<li>
					<figure>
						<img src="http://placehold.it/900x600&text=Project+image" width="450" height="300">
					</figure>

					<aside>
						<h4>Projects title</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nam fugit sapiente tempora incidunt nemo doloribus in vitae consectetur sequi quas officiis ut unde maiores amet laborum deleniti enim assumenda!
						</p>

						<a href="#" class="visit-work">Visit <span class="get-title">Projects title</span></a>
					</aside>
					<div class="clearfix"></div>
				</li>
				<li>
					<figure>
						<img src="http://placehold.it/900x600&text=Project+image" width="450" height="300">
					</figure>

					<aside>
						<h4>Projects title</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nam fugit sapiente tempora incidunt nemo doloribus in vitae consectetur sequi quas officiis ut unde maiores amet laborum deleniti enim assumenda!
						</p>

						<a href="#" class="visit-work">Visit <span class="get-title">Projects title</span></a>
					</aside>
					<div class="clearfix"></div>
				</li>
				<li>
					<figure>
						<img src="http://placehold.it/900x600&text=Project+image" width="450" height="300">
					</figure>

					<aside>
						<h4>Projects title</h4>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, nam fugit sapiente tempora incidunt nemo doloribus in vitae consectetur sequi quas officiis ut unde maiores amet laborum deleniti enim assumenda!
						</p>

						<a href="#" class="visit-work">Visit <span class="get-title">Projects title</span></a>
					</aside>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>

	</div>

	<div id="about">
		<div class="container">
			<article>
				<h3>About me</h3>

				<hr />

				<figure>
					<img src="images/johnie.jpg" alt="Johnie Hjelm" class="profileImg">
				</figure>

				<p>I'm a 21 year old guy born and raised in in Sweden's oldest and first town Sigtuna, where I still live. I graduated in 2011 at ArlandaGymnasiet where I studied IT.</p>

				<p>During my time at ArlandaGymnasiet I started developing my pashion and skills for Front-end development. Since my graduation I worked as a freelancing Front-end Developer, where my main tasks have been designing web sites for smaller companies and startups.</p>
	
				<p>I am a firm believer in well crafted design that stands the test of time, meaning everything I create is thought out, nurtured and cared for to make sure that it is the best possible product I can produce.</p>

				<p>I handcraft CSS, HTML, JavaScript &amp; jQuery documents. I have also a passion for user interface, experience design and accessibility and try to show some of my work on <a href="http://dribbble.com/johnie">Dribbble</a>.</p>

				<p>When I’m not writing code or pushing pixels, I devour everything in my iBooks. I also like listening to blues, traveling and watching TV series.</p>

				<div id="tabs">
					<ul id="tab-links">
						<li><a href="#tab-1">Colophon</a></li>
						<li><a href="#tab-2">Trivia</a></li>
					</ul>
					
					<div id="tab-1">
	
						<p>I’m quite proud of the code I produce. Please feel free to check out the source which are in a <a href="https://bitbucket.org/johnie/hire-johnie">BitBucket repo of mine.</a></p>
		
						<ul> 
							<li>It’s all <strong>HTML5 and CSS3</strong>, and looks good on tablets and mobiles.</li> 
							<li>The typefaces used are <strong>Open Sans</strong> and it's provided by <strong>Google Webfonts</strong>.</li> 
							<li>I’ve build the site around a simple grid, all powered by <strong>SUSY</strong>. <a href="#" class="show-grid">Display grid ›</a></li> 
							<li><strong>SCSS</strong> for CSS preprocessing.</li> 
							<li><strong>CoffeeScript</strong> for the very small amount of Javascript in this site.</li> 
						</ul>
		
						<h5>Tools</h5>
		
						<ul> 
							<li>I’m a Mac. Always will be. I’ve created everything on a <strong>MacBook Pro Retina</strong>.</li> 
							<li>Tested on an <strong>iPad mini</strong> and <strong>iPhone 5</strong>.</li> 
							<li>I use <strong>Sublime Text 2</strong> for code.</li> 
							<li><strong>Photoshop and Illustrator</strong> for image assets.</li> 
							<li><strong>MAMP</strong> for a local PHP/MySQL dev server.</li> 
							<li><strong>Google Chrome</strong> for debugging.</li> 
							<li><strong>Spotify</strong> for relaxation.</li> 
							<li><strong>Twitter</strong> for procrastination.</li> 
							<li><strong>Terminal</strong> for everything else.</li> 
						</ul>
	
					</div>
	
					<div id="tab-2">
	
						<ul> 
							<li>I enjoy the Swedish archipelago in the summers, where I relax.</li> 
							<li>I am interested in fancy coffee and beer.</li> 
							<li>I like animals.</li> 
							<li>I could see myself work for companies like Apple, Google, 37signals, Spotify.</li> 
							<li>I like to get things done.</li> 
							<li>I like the colours blue, white and black – especially together.</li> 
							<li>I want things organized and working effectively.</li> 
							<li>I never decline a great party.</li> 
							<li>I love the creative process from idea to working product.</li> 
							<li>I believe one should live in a warm climate, and travel to the cold snow – not vice versa.</li> 
							<li>I strive being able to take a mockup or wireframe to pixel-perfect working product.</li> 
							<li>I love watching the source code at apple.com and admire the beautiful architecture in their CSS</li>
						</ul>
		
						<h5>Books I've enjoyed</h5>
		
						<ul>
							<li>Steve Jobs</li>
							<li>Larry King Biography</li>
							<li>Design is a job - Mike Montiero</li>
							<li>Start with why - Simon Sinek</li>
							<li>Drive - Daniel Pink</li>
						</ul>
		
						<h5>Music I like</h5>
		
						<ul>
							<li>Stevie Wonder</li>
							<li>Paul Simon</li>
							<li>Ray Charles</li>
							<li>Led Zeppelin</li>
							<li>James Brown</li>
							<li>Turbonegro</li>
						</ul>

						<p>Check out <a href="http://open.spotify.com/user/johniehjelm">my Spotify profile</a> for more ›</p>
	
					</div>
				</div>

			</article>
		</div>
		<div class="clearfix"></div>
	</div>

	<div id="cv">
		<div class="container">
			<h3>Curriculum Vitae</h3>

			<hr />

			<div id="main">
				<strong>Contact</strong>

				<p><span>Email - </span> <em class="email">johniehjelm [snabela] me [punkt] com</em></p>
				<p><span>Phone - </span> 0735 - 12 73 21</p>
				<p><span>Address - </span> Österby by 8, 193 91, Sigtuna</p>

				<strong>Statement</strong>

				<ul class="no-side">
					<li>A flexible and competent front-end developer who is able to work within tight deadlines with high self-motivation, a passion for design, user experience and all things digital.</li>
					<li>A love of learning and further developing an ever expanding design and code knowledge.</li>
					<li>Open, honest, direct and comfortable in giving and receiving constructive feedback.</li>
					<li>Confident and articulate presenter both internally and to clients.</li>
				</ul>

				<strong>Experience</strong>

				<time>
					Nov 10 -
					Present
				</time>

				<p class="with-side">
					<small>
						Genius Lab <a href="http://geniuslab.se">∞</a>
						<span>Front-end Developer / Web Consultant</span>
					</small>
					
					Working as a freelancer can sometimes be very tough. Therefor the startup Genius Lab, were I collaborated with my dear friend and colleague <a href="http://geco.nu" title="Sebastian Brinkenfeldt - Webbdesigner och Webbkonsult">Sebastian Brinkenfeldt</a> to take on some bigger projects. My main tasks were to build the web sites and consult with the company about social networks.
				</p>
		
				<div class="clearfix"></div>

				<time>
					Aug 11 -
					Present
				</time>

				<p class="with-side">
					<small>
						RadioTeknik <a href="http://radioteknik.se">∞</a> <i class="sep">R.T. Trading AB</i>
						<span>Front-end Developer / Web Consultant</span>
					</small>
					Radioteknik is a car supplies, gadgets and breathalyzer shop. Their previous tasks has been to install car- alarms, stereos, gps-trackers etc. Although when car manufactures started to install those gadgets in the production plants, the company restructured to sell more personalized gadgets. Radioteknik now focus on selling top quality breathalyzers, speed camera warners and GPS.

					<br /><br />

					At Radioteknik I was hired as a project employee with tasks as Front-end developer. I built various promotion sites for certain products that they wanted to highlight the most. I also administered their web shop were I updated the stock, added more products and managed the costumer support. As a web consultant I gave feedback on how they could improve their visibility by using social networks.
				</p>
		
				<div class="clearfix"></div>

				<time>
					Aug 11 -
					Dec 11
				</time>

				<p class="with-side">
					<small>
						Trailerland
						<span>Content manager / minor front-end</span>
					</small>
					
					Trailerland is/was a company that leased and sold trailers. Trailers, wether they were for horses or other various loads.

					
				</p>
		
				<div class="clearfix"></div>
				
				<strong>Commission of trust</strong>

				<time>
					Aug 09 -
					Jun 11
				</time>

				<p class="with-side">
					<small>
						ArlandaGymnasiet Märsta
						<span>IT-support</span>
					</small>
					As a commission of trust I work as IT support. The tasks were to help students and teachers who were having problems with their computers, but also to provide classes in the IT program for various information that came out.
				</p>
		
				<div class="clearfix"></div>
			</div>

			<aside>
				<strong>Skills</strong>

				<ul class="no-side">
					<li><b>Things I love to tinker with</b></li>
					<li>Responsive Web Design</li>
					<li>Development</li>
					<li>Page Layout</li>
					<li>Image Manipulation</li>
					<li>Accessability</li>
					<li>Graphic design, <i class="sep">i.e. Posters, T-Shirts etc</i></li>
					<br>
					<li><b>Development Languages I know</b></li>
					<li>HTML5 <i class="sep">HAML</i></li>
					<li>CSS3, <i class="sep">SASS</i></li>
					<li>PHP, <i class="sep">WordPress, Laravel</i></li>
					<li>JavaScript/jQuery</li>
					<li>CoffeeScript</li>
					<li>Git</li>
					<br>
					<li><b>Design</b></li>
					<li>Photoshop</li>
					<li>Illustrator</li>
					<li>InDesign</li>
					<br>
					<li><b>What I'm currently learing</b></li>
					<li>NodeJS</li>
					<li>Ruby</li>
					<br>
					<li><b>Miscellaneous</b></li>
					<li>Expert knowledge about, <i class="sep">Mac &amp; PC</i></li>
				</ul>

				<strong>Qualifications</strong>

				<ul class="no-side">
					<li><b>ArlandaGymnasiet Märsta 08-11</b></li>
					<li>IT, <i class="sep">Web Design, Java, mySQL, Graphic Communication.</i></li>
				</ul>

				<div class="clearfix"></div>

				<strong>Linguistic knowledge</strong>

				<ul class="no-side">
					<li>Swedish, <i class="sep">native language</i></li>
					<li>English, <i class="sep">fluently</i></li>
					<li>German, <i class="sep">basic</i></li>
				</ul>

				<strong>People I've worked with</strong>

				<strong>References</strong>

				<p style="padding-left:15%;">Available on request</p>
			</aside>
		</div>
	</div>

	<footer id="footer">
		<div class="container">
			<cite>
				<?php echo "The year is " . "<strong>" . date('Y') . "</strong>"; ?> <span class="sep">|</span> <a href="#top" id="scrollTop">We have lift-off! <img src="images/spaceship.svg" alt="Spaceship"></a>
			</cite>

			<div class="twitter">
				<a href="https://twitter.com/JohnieHjelm" class="twitter-follow-button" data-show-count="false" data-size="large" data-show-screen-name="false">Follow @JohnieHjelm</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			</div>
		</div>
		<div class="clearfix"></div>
	</footer>
	
	<script type="text/javascript" src="js/master.js"></script>
	<script type="text/javascript" src="js/retina.js"></script>	
</body>
</html>